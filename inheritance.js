var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Person = (function () {
    function Person() {
    }
    Object.defineProperty(Person.prototype, "nme", {
        get: function () {
            return this._nme;
        },
        set: function (nm) {
            this._nme = nm;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "age", {
        get: function () {
            return this._age;
        },
        set: function (age) {
            this._age = age;
        },
        enumerable: true,
        configurable: true
    });
    Person.prototype.getFullDetails = function () {
        return "Age of " + this._nme + " is " + this._age;
    };
    Person.prototype.getDetails = function () {
        return this.getFullDetails();
    };
    return Person;
}());
var Student = (function (_super) {
    __extends(Student, _super);
    function Student() {
        return _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Student.prototype, "rollno", {
        get: function () {
            return this._rollno;
        },
        set: function (rollno) {
            this._rollno = rollno;
        },
        enumerable: true,
        configurable: true
    });
    return Student;
}(Person));
var Teacher = (function (_super) {
    __extends(Teacher, _super);
    function Teacher() {
        return _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Teacher.prototype, "salary", {
        get: function () {
            return this._salary;
        },
        set: function (sl) {
            this._salary = sl;
        },
        enumerable: true,
        configurable: true
    });
    return Teacher;
}(Person));
var std = new Student();
std.age = 20;
std.nme = "Varun";
std.rollno = 1102;
var tch = new Teacher();
tch.age = 25;
tch.nme = "Kamal";
tch.salary = -25000;
// console.log(std.rollno);
// console.log(tch.salary);
console.log(std.getDetails());
