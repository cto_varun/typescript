class Animal
{
    nm: string;
    setName(n: string): void
    {
        this.nm = n;
    }

    getName(): string
    {
        return this.nm;
    }
}


let cow = new Animal();
let tiger = new Animal();
tiger.setName("tiger");
cow.setName("cow");
let cowRes = cow.getName();
let tigerRes = tiger.getName();
console.log(`Cow has ${cowRes} name
            Tiger has ${tigerRes} name
`);
