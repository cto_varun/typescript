var Animal = (function () {
    function Animal() {
    }
    Animal.prototype.setName = function (n) {
        this.nm = n;
    };
    Animal.prototype.getName = function () {
        return this.nm;
    };
    return Animal;
}());
var cow = new Animal();
var tiger = new Animal();
tiger.setName("tiger");
cow.setName("cow");
var cowRes = cow.getName();
var tigerRes = tiger.getName();
console.log("Cow has " + cowRes + " name\n\n            Tiger has " + tigerRes + " name\n");
