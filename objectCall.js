var Animal = (function () {
    function Animal(nm) {
        if (nm == undefined) {
            this.nme = "Default name";
        }
        else {
            this.nme = nm;
        }
    }
    Animal.prototype.showName = function () {
        return this.nme;
    };
    return Animal;
}());
var cow = new Animal();
console.log(cow.showName());
